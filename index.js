const app = {
	data() {
		return {
			version: '2023-10-30',
			list: [{
				name: 'iOS版',
				url: './release/music-ipa.ipa',
				title: '点击下载',
				icon: './res/apple.png',
			}, {
				name: 'Android版',
				url: './release/music-apk.apk',
				title: '点击下载',
				icon: './res/android.png',
			}, {
				name: 'macOS版',
				url: './release/music-app.zip',
				title: '点击下载',
				icon: './res/apple.png',
			}, {
				name: 'Windows版',
				url: './release/music-exe.zip',
				title: '点击下载',
				icon: './res/windows.png',
			}, {
				name: 'Linux版',
				url: './release/music-tar.zip',
				title: '点击下载',
				icon: './res/linux.png',
			}, {
				name: 'Web体验版',
				url: './pages/music.html',
				title: '点击体验',
				icon: './res/explorer.png',
			}]
		}
	},
	methods: {

	},
	watch: {

	},
	mounted() {

	}
}
Vue.createApp(app).mount('#body')


window.onload = function() {

}