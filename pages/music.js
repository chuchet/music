const app = {
	data() {
		return {
			singers: [{
					"singer": "许嵩",
					"img": "https://mp3.haoge500.com/9kuimg/pic/music/2023/03-26/1654318.jpg"
				}, {
					"singer": "徐良",
					"img": "https://mp3.haoge500.com/pic/gstx/2/17965.jpg"
				},
				{
					"singer": "汪苏泷",
					"img": "https://mp3.haoge500.com/pic/gstx/1/8929.jpg"
				},
				{
					"singer": "周杰伦",
					"img": "https://mp3.haoge500.com/9kuimg/pic/music/2023/03-26/1649919.jpg"
				},
				{
					"singer": "林俊杰",
					"img": "https://mp3.haoge500.com/9kuimg/pic/music/2023/03-25/1642823.jpg"
				},
				{
					"singer": "星弟",
					"img": "https://mp3.haoge500.com/pic/gstx/1/7752.jpg"
				},
				{
					"singer": "后弦",
					"img": "https://mp3.haoge500.com/pic/gstx/1/2363.jpg"
				},
				{
					"singer": "周传雄",
					"img": "https://mp3.haoge500.com/9kuimg/pic/music/2023/03-25/1643128.jpg"
				},
				{
					"singer": "本兮",
					"img": "https://mp3.haoge500.com/pic/gstx/2/12104.jpg"
				},
				{
					"singer": "张韶涵",
					"img": "https://mp3.haoge500.com/9kuimg/pic/music/2023/03-21/1596337.jpg"
				},
				{
					"singer": "张靓颖",
					"img": "https://mp3.haoge500.com/9kuimg/pic/music/2023/02-01/1082681.jpg"
				},
				{
					"singer": "张杰",
					"img": "https://mp3.haoge500.com/9kuimg/pic/music/2023/03-21/1596233.jpg"
				},
				{
					"singer": "白小白",
					"img": "https://mp3.haoge500.com/9kuimg/pic/music/2021/12-09/1371040.jpg"
				},
				{
					"singer": "Tank",
					"img": "https://mp3.haoge500.com/9kuimg/pic/music/2023/03-21/467894.jpg"
				},
				{
					"singer": "单色凌",
					"img": "https://mp3.haoge500.com/pic/gstx/2/18744.jpg"
				},
				{
					"singer": "格子兮",
					"img": "https://mp3.haoge500.com/pic/gstx/5/47175.jpg"
				},
				{
					"singer": "乔洋",
					"img": "https://mp3.haoge500.com/pic/gstx/1/7250.jpg"
				},
				{
					"singer": "刀郎",
					"img": "https://mp3.haoge500.com/9kuimg/pic/music/2023/03-26/1657251.jpg"
				},
				{
					"singer": "T.R.Y.",
					"img": "https://mp3.haoge500.com/pic/gstx/13/127894.jpg"
				},
				{
					"singer": "乌兰托娅",
					"img": "https://mp3.haoge500.com/9kuimg/geshou/20160630/380d5ad655d0ea98.jpg"
				}
			],
			musics: [{
					"name": "有何不可",
					"singer": "许嵩",
					"album": "自定义",
					"url": "https://mp3.haoge500.com/hot/2008/12-11/177725.mp3"
				},
				{
					"name": "幻听",
					"singer": "许嵩",
					"album": "梦游计",
					"url": "https://mp3.haoge500.com/hot/2012/06-29/468406.mp3"
				},
				{
					"name": "半城烟沙",
					"singer": "许嵩",
					"album": "半城烟沙",
					"url": "https://mp3.haoge500.com/hot/2010/04-12/216495.mp3"
				},
				{
					"name": "素颜",
					"singer": "许嵩",
					"album": "素颜",
					"url": "https://mp3.haoge500.com/upload/rank/20211218/df711f4a47e55705874b32ba7fdfa4af.mp3"
				},
				{
					"name": "如果当时",
					"singer": "许嵩",
					"album": "自定义",
					"url": "https://mp3.haoge500.com/hot/2008/12-21/178263.mp3"
				},
				{
					"name": "千百度",
					"singer": "许嵩",
					"album": "苏格拉没有底",
					"url": "https://mp3.haoge500.com/upload/rank/20211220/b057561404ba5ec78f30589c2230b96c.mp3"
				},
				{
					"name": "城府",
					"singer": "许嵩",
					"album": "自定义",
					"url": "https://mp3.haoge500.com/upload/rank/20220209/ac18d9ecff56513196995bd3b93e21ff.mp3"
				},
				{
					"name": "玫瑰花的葬礼",
					"singer": "许嵩",
					"album": "V",
					"url": "https://mp3.haoge500.com/hot/2007/01-21/80753.mp3"
				},
				{
					"name": "灰色头像",
					"singer": "许嵩",
					"album": "寻雾启示",
					"url": "https://mp3.haoge500.com/upload/rank/20220123/ee79e955d3b752fa8c7c1ff304f4eea4.mp3"
				},
				{
					"name": "清明雨上",
					"singer": "许嵩",
					"album": "自定义",
					"url": "https://mp3.haoge500.com/upload/rank/20220212/6d54da9858bc5a0cb3d123c0800a1e95.mp3"
				},
				{
					"name": "认错",
					"singer": "许嵩",
					"album": "自定义",
					"url": "https://mp3.haoge500.com/upload/rank/20211215/cd355950acac594eb9948dfa1da309ff.mp3"
				},
				{
					"name": "天龙八部之宿敌",
					"singer": "许嵩",
					"album": "《天龙八部之宿敌》游戏原声",
					"url": "https://mp3.haoge500.com/upload/rank/20211215/78a136a03de955648de3d8ad4e498884.mp3"
				},
				{
					"name": "忧伤歌声",
					"singer": "许嵩",
					"album": "自定义",
					"url": "https://mp3.haoge500.com/hot/2009/02-02/179475.mp3"
				},
				{
					"name": "你若成风",
					"singer": "许嵩",
					"album": "乐酷",
					"url": "https://mp3.haoge500.com/upload/rank/20211221/71139d809fc55efe9270e8f9dcf83a6d.mp3"
				},
				{
					"name": "多余的解释",
					"singer": "许嵩",
					"album": "自定义",
					"url": "https://mp3.haoge500.com/hot/2008/12-31/178565.mp3"
				},
				{
					"name": "情侣装",
					"singer": "许嵩",
					"album": "情侣装 EP",
					"url": "https://mp3.haoge500.com/hot/2008/01-22/94013.mp3"
				},
				{
					"name": "断桥残雪",
					"singer": "许嵩",
					"album": "断桥残雪",
					"url": "https://mp3.haoge500.com/hot/2007/12-29/93483.mp3"
				},
				{
					"name": "温泉",
					"singer": "许嵩",
					"album": "温泉",
					"url": "https://mp3.haoge500.com/upload/rank/20211225/4cf7b5d3bd11574ebbf9a984628202c0.mp3"
				},
				{
					"name": "庐州月",
					"singer": "许嵩",
					"album": "寻雾启示",
					"url": "https://mp3.haoge500.com/upload/rank/20220108/0c95d97b2a5d5b1aaa48c5afd6f6999d.mp3"
				},
				{
					"name": "想象之中",
					"singer": "许嵩",
					"album": "苏格拉没有底",
					"url": "https://mp3.haoge500.com/upload/rank/20220109/b9991bb8945e51198b3a7d94f83f744f.mp3"
				},
				{
					"name": "雅俗共赏",
					"singer": "许嵩",
					"album": "青年晚报",
					"url": "https://mp3.haoge500.com/upload/128/2016/05/19/828813.mp3"
				},

				{
					"name": "安琪",
					"singer": "许嵩",
					"album": "半城烟沙",
					"url": "https://mp3.haoge500.com/upload/rank/20220129/273d5702263f513fbf24183d9e8b1017.mp3"
				},
				{
					"name": "惊鸿一面",
					"singer": "许嵩",
					"album": "不如吃茶去",
					"url": "https://mp3.haoge500.com/upload/rank/20220211/c71173da70a9512a9f536aebe9f48cda.mp3"
				},
				{
					"name": "后会无期",
					"singer": "徐良",
					"album": "不良少年",
					"url": "https://mp3.haoge500.com/upload/rank/20220212/586ad511ef7c5be39c778dc7904da698.mp3"
				},
				{
					"name": "坏女孩",
					"singer": "徐良",
					"album": "不良少年",
					"url": "https://mp3.haoge500.com/upload/rank/20220204/74119295f0f65fbfa58fd97a6c15e3d6.mp3"
				},
				{
					"name": "七秒钟的记忆",
					"singer": "徐良",
					"album": "情话",
					"url": "https://mp3.haoge500.com/hot/2013/01-07/504147.mp3"
				},
				{
					"name": "客官不可以",
					"singer": "徐良",
					"album": "徐良原创合集",
					"url": "https://mp3.haoge500.com/upload/rank/20220209/45168ea8e2da5503997e53004fdf1496.mp3"
				},
				{
					"name": "情话",
					"singer": "徐良",
					"album": "情话",
					"url": "https://mp3.haoge500.com/hot/2013/05-03/510107.mp3"
				},
				{
					"name": "抽离",
					"singer": "徐良",
					"album": "情话",
					"url": "https://mp3.haoge500.com/mp3/561/560069.mp3"
				},
				{
					"name": "和平分手",
					"singer": "徐良",
					"album": "犯贱",
					"url": "https://mp3.haoge500.com/hot/2010/12-21/407510.mp3"
				},
				{
					"name": "天真",
					"singer": "徐良",
					"album": "犯贱",
					"url": "https://mp3.haoge500.com/hot/2011/12-05/417064.mp3"
				},
				{
					"name": "犯贱",
					"singer": "徐良",
					"album": "徐良原创合集",
					"url": "https://mp3.haoge500.com/hot/2010/12-21/407509.mp3"
				},
				{
					"name": "考试什么的都去死吧",
					"singer": "徐良",
					"album": "不良少年",
					"url": "https://mp3.haoge500.com/mp3/462/461659.mp3"
				},
				{
					"name": "飞机场",
					"singer": "徐良",
					"album": "徐良原创合集",
					"url": "https://mp3.haoge500.com/hot/2011/06-14/411861.mp3"
				},
				{
					"name": "红装",
					"singer": "徐良",
					"album": "犯贱",
					"url": "https://mp3.haoge500.com/upload/rank/20220211/7891f28e84c1577a962a6a120a8a39c8.mp3"
				},
				{
					"name": "月光",
					"singer": "徐良",
					"album": "犯贱",
					"url": "https://mp3.haoge500.com/hot/2011/03-16/409547.mp3"
				},

				{
					"name": "虐心",
					"singer": "徐良",
					"album": "情话",
					"url": "https://mp3.haoge500.com/hot/2013/01-17/504476.mp3"
				},
				{
					"name": "星座恋人",
					"singer": "徐良",
					"album": "我",
					"url": "https://mp3.haoge500.com/new/2015/01-15/657572.mp3"
				},
				{

					"name": "小星星",
					"singer": "汪苏泷",
					"album": "慢慢懂",
					"url": "https://mp3.haoge500.com/hot/2010/08-16/366825.mp3"
				},
				{

					"name": "剑魂",
					"singer": "汪苏泷",
					"album": "剑魂",
					"url": "https://mp3.haoge500.com/upload/128/2021/11/15/1234981.mp3"
				},
				{

					"name": "放不下",
					"singer": "汪苏泷",
					"album": "慢慢懂",
					"url": "https://mp3.haoge500.com/hot/2010/02-22/209672.mp3"
				},
				{
					"name": "三国杀",
					"singer": "汪苏泷",
					"album": "汪苏泷原创合集",
					"url": "https://mp3.haoge500.com/hot/2011/05-15/411158.mp3"
				},
				{
					"name": "万有引力",
					"singer": "汪苏泷",
					"album": "汪苏泷原创合集",
					"url": "https://mp3.haoge500.com/hot/2012/06-18/468044.mp3"
				},
				{
					"name": "小流星",
					"singer": "汪苏泷",
					"album": "小流星",
					"url": "https://mp3.haoge500.com/upload/128/2018/06/01/880008.mp3"
				},
				{
					"name": "有点甜",
					"singer": "汪苏泷",
					"album": "万有引力",
					"url": "https://mp3.haoge500.com/hot/2012/06-28/468311.mp3"
				},

				{
					"name": "苦笑",
					"singer": "汪苏泷",
					"album": "好安静",
					"url": "https://mp3.haoge500.com/upload/rank/20220209/fc5440e6a29d53e49a53a7bff9d246ec.mp3"
				},
				{
					"name": "专属味道",
					"singer": "汪苏泷",
					"album": "慢慢懂",
					"url": "https://mp3.haoge500.com/hot/2010/09-03/385919.mp3"
				},

				{
					"name": "不分手的恋爱",
					"singer": "汪苏泷",
					"album": "好安静",
					"url": "https://mp3.haoge500.com/upload/rank/20211214/c640661842bf5d6ca8d1e24a7e105330.mp3"
				},
				{
					"name": "风度",
					"singer": "汪苏泷",
					"album": "汪苏泷原创合集",
					"url": "https://mp3.haoge500.com/upload/rank/20211231/928943e3bb3e5a479a1c56bcdb05f580.mp3"
				},
				{
					"name": "因为了解",
					"singer": "汪苏泷",
					"album": "慢慢懂",
					"url": "https://mp3.haoge500.com/upload/rank/20211230/8b442372bd0159809f35086929814843.mp3"
				},
				{
					"name": "好安静",
					"singer": "汪苏泷",
					"album": "汪苏泷 精选集",
					"url": "https://mp3.haoge500.com/hot/2011/08-06/412736.mp3"
				},
				{
					"name": "如果你还不明白",
					"singer": "汪苏泷",
					"album": "慢慢懂",
					"url": "https://mp3.haoge500.com/hot/2010/11-26/404320.mp3"
				},
				{
					"name": "你让我懂",
					"singer": "汪苏泷",
					"album": "慢慢懂",
					"url": "https://mp3.haoge500.com/hot/2011/05-09/410934.mp3"
				},
				{
					"name": "夜曲",
					"singer": "周杰伦",
					"album": "十一月的萧邦",
					"url": "https://mp3.haoge500.com/hot/2005/11-01/72064.mp3"
				},
				{
					"name": "彩虹",
					"singer": "周杰伦",
					"album": "我很忙",
					"url": "https://mp3.haoge500.com/hot/2007/10-26/91002.mp3"
				},
				{
					"name": "兰亭序",
					"singer": "周杰伦",
					"album": "魔杰座",
					"url": "https://mp3.haoge500.com/hot/2008/10-06/176211.mp3"
				},
				{
					"name": "给我一首歌的时间",
					"singer": "周杰伦",
					"album": "魔杰座",
					"url": "https://mp3.haoge500.com/hot/2008/10-06/176214.mp3"
				},
				{
					"name": "晴天",
					"singer": "周杰伦",
					"album": "叶惠美",
					"url": "https://mp3.haoge500.com/upload/rank/20220209/9159e475339551fda240403f5ad66352.mp3"
				},
				{
					"name": "以父之名",
					"singer": "周杰伦",
					"album": "叶惠美",
					"url": "https://mp3.haoge500.com/hot/2004/07-17/41811.mp3"
				},
				{
					"name": "告白气球",
					"singer": "周杰伦",
					"album": "周杰伦的床边故事",
					"url": "https://mp3.haoge500.com/upload/rank/20220203/5800853f8b0e515eb44ee63f022a4ca9.mp3"
				},
				{
					"name": "稻香",
					"singer": "周杰伦",
					"album": "魔杰座",
					"url": "https://mp3.haoge500.com/hot/2008/09-21/175930.mp3"
				},
				{
					"name": "东风破",
					"singer": "周杰伦",
					"album": "叶惠美",
					"url": "https://mp3.haoge500.com/upload/rank/20211219/64ba6a3bd2005d8794517d14edbf8fc1.mp3"
				},
				{
					"name": "听妈妈的话",
					"singer": "周杰伦",
					"album": "依然范特西",
					"url": "https://mp3.haoge500.com/hot/2006/09-04/78064.mp3"
				},
				{
					"name": "甜甜的",
					"singer": "周杰伦",
					"album": "我很忙",
					"url": "https://mp3.haoge500.com/upload/rank/20211215/887dd9a425db5738a8686043ba4b35e4.mp3"
				},
				{
					"name": "牛仔很忙",
					"singer": "周杰伦",
					"album": "我很忙",
					"url": "https://mp3.haoge500.com/hot/2007/10-11/90149.mp3"
				}
			],
			searchs: [],
			audio: {
				"name": "歌名",
				"singer": "歌手",
				"album": "专辑",
				"url": ""
			},
			isSearch: false,
			resIndex: 0,
			index: 0,
			keyword: "",
		}
	},
	onload() {},
	methods: {
		playClick(playIndex) {
			this.index = playIndex;
			let array = this.isSearch ? this.searchs : this.musics
			this.audio = array[this.index]
			let audio = this.$refs.player;
			if (this.audio.url.startsWith("http")) {
				audio.pause();
				audio.load();
				audio.play();
			}
		},
		finished() {
			this.index += 1;
			let array = this.isSearch ? this.searchs : this.musics
			if (this.index >= array.length) {
				this.index = 0;
			}
			this.playClick(this.index);
		},
		cancelClick() {
			this.keyword = '';
			this.isSearch = false;
		},
		searchClick() {
			this.isSearch = true
		},
	},
	watch: {
		keyword: function(value) {
			var string = value.replaceAll(' ', '');
			if (string.length == 0) {
				this.isSearch = false
			}
		}
	},
	mounted() {}
}
Vue.createApp(app).mount('#body')


window.onload = function() {

}